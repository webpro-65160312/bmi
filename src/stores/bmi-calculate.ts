import { defineStore } from 'pinia'

export const useBmiStore = defineStore('bmi', {
  state: () => ({
    height: 0,
    weight: 0,
    bmiResult: 0
  }),
  actions: {
    calculateBmi() {
      if (isNaN(this.height) || isNaN(this.weight) || this.height <= 0 || this.weight <= 0) {
        this.bmiResult = 0
        return
      }

      const heightInMeters = this.height / 100
      this.bmiResult = parseFloat((this.weight / (heightInMeters * heightInMeters)).toFixed(2))
    },
    copyToClipboard() {
      const el = document.createElement('textarea')
      el.value = this.bmiResult.toString()
      document.body.appendChild(el)
      el.select()
      document.execCommand('copy')
      document.body.removeChild(el)
    }
  }
})
